let tabs, gallery;
function api(endpoint,data = null, method = 'GET') {
    let url = 'https://tourscanner.com/interview/';
    url += endpoint;
    return new Promise((resolve, reject) => {
        $.ajax({
            url: url,
            type: method,
            data: data,
            dataType: "json",
            success: function (data) {
                resolve(data)
            },
            error: function (error) {
                console.log(error);
                reject(error)
            },
        })
    })
}

function createTabs() {
    tabs = $('<ul>')
        .attr({id:'tabs'})
        .append(
            function () {
                let items = [];
                items.push(createTab('','all images',true));
                let folders = localStorage.getItem('bookmark') ? JSON.parse(localStorage.getItem('bookmark')) : [];
                folders.forEach(folder => {
                    items.push(createTab(folder.name,folder.name));
                });
                return items;
            }
        )
    return tabs;
}
function createTab(folder,name,isActive = false) {
    return $('<li>')
        .data({folder: folder})
        .addClass(isActive ? 'active': null)
        .html(name)
        .click(function(){
            if(!$(this).hasClass('active')) {
                tabs.children().removeClass('active');
                $(this).addClass('active');
                if(folder) {
                    gallery.children().hide();
                    gallery.children().each(function(){
                        if($(this).data('folder') == folder) {
                            $(this).show();
                        }
                    });
                }
                else {
                    gallery.children().show();
                }

            }
        })
}

function createGallery(items) {
    gallery = $('<ul>')
        .attr({id: 'gallery'})
        .append(function(){
            api('images').then(images => {
                images.forEach(item => {
                    let bookmarked = checkBookmark(item.image_id);
                    gallery.append(
                        $('<li>')
                            .data({'folder': bookmarked})
                            .addClass(bookmarked ? 'bookmarked' : null)
                            .append(
                                $('<div>').append(
                                    $('<img>')
                                        .attr({id: 'img-' + item.image_id, src: item.url})
                                        .addClass(!bookmarked ? 'bookmarkable' : null)
                                        .click(function () {
                                            if ($(this).hasClass('bookmarkable')) {
                                                bookmark(item.image_id).then(folder => {
                                                    if (folder) {
                                                        $(this).parent().parent().addClass('bookmarked');
                                                        $(this).parent().parent().data({'folder': folder});
                                                        $(this).removeClass('bookmarkable');
                                                    }
                                                });
                                            }
                                        })
                                ),
                                $('<div>').append(
                                    $('<i>').addClass('arrow'),
                                    $('<p>').html(item.title)
                                )
                            )
                    );
                });
            });
        });

    return gallery;
}

function bookmark(id) {
    return api('save_image/'+id).then(times => {
        let folder = prompt("This image has been saved "+times+" times.\nPlease enter the name of the folder where you want to save the image");
        if(folder === null) {
            return false;
        }
        if(folder) {
            return api('save_image/'+id, null, 'POST').then(response => {
                if(response.success) {
                    return saveFolder(folder,id);
                }
                else {
                    alert('An error occurred.\n Please try again');
                    return false;
                }
            });
        }
        else {
            alert('Please provide a folder name');
            return false;
        }
        return false;
    });
    return false;
}

function saveFolder(name,id) {
    let folders = localStorage.getItem('bookmark') ? JSON.parse(localStorage.getItem('bookmark')) : [];
    let folderIndex = folders.findIndex(folder => {
        return folder.name == name;
    });
    if(folderIndex < 0) {
        let folder = {
            name: name,
            items: [id]
        }
        folders.push(folder);
        localStorage.setItem('bookmark',JSON.stringify(folders));

        tabs.append(createTab(folder.name,folder.name));
    }
    else {
        let folder = folders[folderIndex];
        let imageIndex = folder.items.findIndex(item => {
            return item == id;
        });
        if(imageIndex < 0) {
            folder.items.push(id);
            folders[folderIndex] = folder;
            localStorage.setItem('bookmark',JSON.stringify(folders));
        }
    }
    return name;

}

function checkBookmark(id) {
    let folders = localStorage.getItem('bookmark') ? JSON.parse(localStorage.getItem('bookmark')) : [];
    let name = null;
    folders.forEach(folder => {
        let index = folder.items.findIndex(item => {
            return item == id;
        });
        if(index > -1) {
            name = folder.name;
        }
    });
    return name;
}

$( document ).ready(function() {
    $('body').append(
        $('<nav>').append(() => {
            return createTabs()
        }),
        $('<section>').append(() => {
            return createGallery();
        })
    );
    $(window).scroll(function(){
        if($(this).scrollTop() > $('nav').height()) {
            $('nav').addClass( 'scrolled' );
        }
        else {
            $('nav').removeClass('scrolled');
        }
    });
});