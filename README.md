# README #

This repository contains gallery project as requested. There are 2 galleries
* pure javascript gallery
* jquery gallery


### Why wasn't php used? ###

The requirements make the use of php superfluous. XHR calls could point to a php page which could use curl to make calls to the provided endpoint. A little redundant? Unlike cookies, it is not possible to access the localStorage in php so the entire procedure for saving folders is delegated to javascript.

### Why aren't frameworks like bootstrap, react, angular used? ###

For the required project you would write too much code using React or Angular compared to pure javascript or jquery

It would be conceivable to use bootstrap in order to access its grid layout system, modal for the prompt and nav for the tabs.
2% of the framework would be used against the weight of the css and js files.
