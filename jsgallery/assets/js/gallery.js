const myStorage = window.localStorage;
const tabs = document.getElementById('tabs');
const gallery = document.getElementById('gallery');

function api(endpoint,data = null, method = 'GET') {
    let url = 'https://tourscanner.com/interview/';
    url += endpoint;
    return fetch(url, {
        method: method,
        headers: {
            'Accept': 'application/json'
        },
        body: data
    })
    .then((response) => response.json())
    .then((responseData) => {
        return responseData
    })
    .catch((err) => {
        console.warn(err);
    });

}

function populateTabs() {
    tabs.append(createTab('','All images', true));
    let folders = myStorage.getItem('bookmark') ? JSON.parse(myStorage.getItem('bookmark')) : [];
    folders.forEach(folder => {
        tabs.append(createTab(folder.name,folder.name));
    });

}
function createTab(folder,name,isActive = false) {
    let tab = document.createElement("li");
    tab.dataset.folder = folder;
    tab.innerHTML = name;
    if(isActive) {
        tab.classList.add('active');
    }
    tab.addEventListener('click', function () {
        if (!this.classList.contains('active')) {
            viewFolderImages(this.dataset.folder);
        }
    });
    return tab;
}
function viewFolderImages(bookmark) {
    let containers = document.getElementById("gallery").getElementsByTagName("li");
    let tabs = document.getElementById("tabs").getElementsByTagName("li");
    for (let item of containers) {
        item.style.display = bookmark != '' ? item.dataset.folder == bookmark ? 'grid' : 'none' : 'grid';
    }
    for (let item of tabs) {
        item.classList.remove('active');
        if(item.dataset.folder == bookmark) {
            item.classList.add('active');
        }
    }
}

function createGallery(items) {
    items.forEach(item => {
        let li = document.createElement("li");
        let bookmarked = checkBookmark(item.image_id);
        li.dataset.folder = bookmarked || '';

        let imageContainer = document.createElement("div");
        let image = document.createElement("img");
        image.src = item.url;
        image.id = 'img-'+item.image_id;
        if(!bookmarked) {
            image.classList.add('bookmarkable');
            image.addEventListener('click', function () {
                if(this.classList.contains('bookmarkable')) {
                    bookmark(item.image_id).then(folder => {
                        if(folder) {
                            image.parentElement.parentElement.classList.add('bookmarked');
                            image.parentElement.parentElement.dataset.folder = folder;
                            this.classList.remove('bookmarkable');
                        }
                    });
                }
            })
        }
        else {
            li.classList.add('bookmarked');
        }
        imageContainer.append(image);
        let infoContainer = document.createElement("div");
        let arrow = document.createElement("i");
        let title = document.createElement("p");
        title.innerText = item.title;
        infoContainer.append(arrow,title)

        li.append(imageContainer,infoContainer);
        gallery.append(li);
    })
}

function bookmark(id) {
    return api('save_image/'+id).then(times => {
        let folder = prompt("This image has been saved "+times+" times.\nPlease enter the name of the folder where you want to save the image");
        if(folder === null) {
            return false;
        }
        if(folder) {
            return api('save_image/'+id, null, 'POST').then(response => {
                if(response.success) {
                    return saveFolder(folder,id);
                }
                else {
                    alert('An error occurred.\n Please try again');
                    return false;
                }
            });
        }
        else {
            alert('Please provide a folder name');
            return false;
        }
        return false;
    });
    return false;
}

function saveFolder(name,id) {
    let folders = myStorage.getItem('bookmark') ? JSON.parse(myStorage.getItem('bookmark')) : [];
    let folderIndex = folders.findIndex(folder => {
        return folder.name == name;
    });
    if(folderIndex < 0) {
        let folder = {
            name: name,
            items: [id]
        }
        folders.push(folder);
        myStorage.setItem('bookmark',JSON.stringify(folders));

        tabs.append(createTab(folder.name,folder.name));
    }
    else {
        let folder = folders[folderIndex];
        let imageIndex = folder.items.findIndex(item => {
            return item == id;
        });
        if(imageIndex < 0) {
            folder.items.push(id);
            folders[folderIndex] = folder;
            myStorage.setItem('bookmark',JSON.stringify(folders));
        }
    }
    return name;

}

function checkBookmark(id) {
    let folders = myStorage.getItem('bookmark') ? JSON.parse(myStorage.getItem('bookmark')) : [];
    let name = null;
    folders.forEach(folder => {
        let index = folder.items.findIndex(item => {
            return item == id;
        });
        if(index > -1) {
            name = folder.name;
        }
    });
    return name;
}

function scrolling() {
    let tabsHeight = tabs.offsetHeight+1;
    if(document.body.scrollTop > tabsHeight || document.documentElement.scrollTop > tabsHeight) {
        tabs.parentElement.classList.add('scrolled');
    }
    else {
        tabs.parentElement.classList.remove('scrolled');
    }
    //console.log(tabs.offsetHeight,document.body.scrollTop,document.documentElement.scrollTop);
}

populateTabs();
api('images').then(items => {
    createGallery(items);
});
document.onscroll = function() {scrolling()};
